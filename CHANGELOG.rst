ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

0.5.1 (2022-10-04)
------------------

* Fix encoding of Raiffeisenbank statements

0.5.0 (2022-10-04)
------------------

* Drop support for python 3.5 and 3.6 and zeep 2.
* Add support for python 3.10.
* Ignore Raiffeisenbank subaccounts in statements (#1).
* Update documentation.
* Update static checks.

0.4.0 (2021-07-19)
------------------
* Do not support TextIO or BytesIO interface in RawStatement any more. Use it as a data class instead.
* Do not convert downloaded data to str in downloaders. Use binary data in RawStatement instead.

0.3.7 (2021-07-13)
------------------
Add capability to retry download when files are not ready to CSOBStatementDownloader.

0.3.6 (2021-06-29)
------------------
Use BIC instead of bank code if bank code is missing in FioParser.

0.3.5 (2021-04-14)
------------------
Check slash at the end of base_url parameter in Downloaders.

0.3.4 (2021-04-08)
------------------
Change BankStatementDownloader interface to return Sequence instead of Iterable.

0.3.3 (2021-03-23)
------------------
Allow account filtering for CSOBStatementDownloader.

0.3.2 (2021-03-04)
------------------

Fix changelog

0.3.1 (2021-02-11)
------------------

Incorporate changes from 0.2.1.

0.3.0 (2021-02-09)
------------------

Allow GPC statement download from CSOB.

* Allow filtering by statement type and frequency for CSOB
* Use datetime instead of date in API calls
* Return statements as RawStatement data class instead of string

0.2.2 (2021-03-04)
------------------

Fix changelog

0.2.1 (2021-02-11)
------------------

Use older versions of modules.

* Use older version of requests
* Use older version of zeep

0.2.0 (2021-02-01)
------------------

Add Raiffeisen and CSOB downloaders and parsers.

* Add Raiffeisen downloader.
* Add CSOB downloader.
* Add Raiffeisen parser
* Add CSOB parser
* Add JSON import/export
* Implement eq for Payment data class
* Remove data classes validation
* Remove whitespace from empty statements in Raiffeisen downloader
* Return None for incomplete account number
* Consistent treatment of empty strings and Nones in account numbers
* Declare type annotation support

0.1.0 (2020-10-29)
------------------

Initial version.

* BankStatementDownloader and BankStatementParser interfaces
* BankStatement and Payment data classes
* Implementations for CSOB, Fio, and Raiffeisen
