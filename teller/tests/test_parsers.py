#
# Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
from collections import OrderedDict
from datetime import date, datetime
from io import BytesIO
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Mapping, Optional
from unittest import TestCase

from moneyed import Money

from teller.parsers import BankStatementParser, CSOBParser, FioParser, RaiffeisenParser
from teller.statement import Payment


class TestBankStatementParser(TestCase):
    def test_parse_raises_exception(self):
        with self.assertRaisesRegex(NotImplementedError, 'This is abstract base class'):
            BankStatementParser.parse_string('')

        with self.assertRaisesRegex(NotImplementedError, 'This is abstract base class'):
            BankStatementParser.parse_file('/tmp/a.txt')


class TestFioParser(TestCase):

    path = Path('teller/tests/data/example_fio.xml')
    xml_template = Path('teller/tests/data/test_template_fio.xml')

    def test_parse_header_path(self):
        statement = FioParser.parse_file(self.path)
        self.assertEqual(statement.account_number, '1234567890/2010')

    def test_parse_header_file(self):
        with self.path.open('rb') as f:
            statement = FioParser.parse_file(f)
        self.assertEqual(statement.account_number, '1234567890/2010')

    def test_parse_header_str(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '<TransactionList></TransactionList>'
        xml += '</AccountStatement>'

        statement = FioParser.parse_string(xml)
        self.assertEqual(statement.account_number, '1234567890/2010')

    def test_parse_header_bytesio(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '<TransactionList></TransactionList>'
        xml += '</AccountStatement>'

        statement = FioParser.parse_file(BytesIO(xml.encode('utf-8')), encoding='utf-8')
        self.assertEqual(statement.account_number, '1234567890/2010')

    def test_missing_header(self):
        xml = '<AccountStatement></AccountStatement>'
        with self.assertRaisesRegex(ValueError, 'Statement is missing header\\.'):
            FioParser.parse_string(xml)

    def test_missing_account_number(self):
        xml = '<AccountStatement><Info></Info></AccountStatement>'
        with self.assertRaisesRegex(ValueError, 'Could not parse account number\\.'):
            FioParser.parse_string(xml)

    def test_parse_payment(self):
        statement = FioParser.parse_file(self.path)

        payment = statement.payments[1]

        self.assertEqual(payment.identifier, '5800012966')
        self.assertEqual(payment.transaction_date, date(2012, 12, 21))
        self.assertEqual(payment.counter_account, '3456789012/2010')
        self.assertEqual(payment.name, 'Jan Novak')
        self.assertEqual(payment.amount, Money(3000, 'CZK'))
        self.assertEqual(payment.description, 'Veselé Vánoce <> & Nový rok')
        self.assertEqual(payment.constant_symbol, '0234')
        self.assertEqual(payment.variable_symbol, '222222')
        self.assertEqual(payment.specific_symbol, '333333')

    def test_parse_multiple_payments(self):
        statement = FioParser.parse_file(self.path)
        self.assertEqual(len(statement.payments), 2)
        self.assertEqual(sum(p.amount for p in statement.payments), Money(4000, 'CZK'))

    def test_parse_payment_BIC(self):
        # Convert to str for python 3.5
        with open(str(self.xml_template)) as f:
            xml_template = f.read()

        bic = '<column_26 name="BIC" id="26">ABC123</column_26>'
        test_data = (
            (bic, '3456789012/ABC123'),
            (bic + '<column_3 id="3" name="Kód banky"></column_3>', '3456789012/ABC123'),
            (bic + '<column_3 id="3" name="Kód banky">2010</column_3>', '3456789012/2010'),
        )

        for bank_code, result in test_data:
            with self.subTest(result=result):
                xml = xml_template.format(bank_code=bank_code)
                statement = FioParser.parse_string(xml)

                payment = statement.payments[0]

                self.assertEqual(payment.identifier, '5800012966')
                self.assertEqual(payment.counter_account, result)
                self.assertEqual(payment.name, 'Jan Novak')
                self.assertEqual(payment.amount, Money(3000, 'CZK'))

    def test_no_transactions(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '<TransactionList></TransactionList>'
        xml += '</AccountStatement>'

        statement = FioParser.parse_string(xml)
        self.assertEqual(len(statement.payments), 0)

    def test_no_transaction_list(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '</AccountStatement>'

        statement = FioParser.parse_string(xml)
        self.assertEqual(len(statement.payments), 0)

    def test_empty_transaction(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '<TransactionList><Transaction></Transaction></TransactionList>'
        xml += '</AccountStatement>'

        statement = FioParser.parse_string(xml)
        self.assertEqual(statement.payments[0].amount, None)
        self.assertEqual(statement.payments[0].counter_account, None)
        self.assertEqual(statement.payments[0].transaction_date, None)

    def test_invalid_date_raises_exception(self):
        xml = '<AccountStatement><Info>'
        xml += '<accountId>1234567890</accountId><bankId>2010</bankId>'
        xml += '</Info>'
        xml += '<TransactionList><Transaction><column_0>not a date</column_0></Transaction></TransactionList>'
        xml += '</AccountStatement>'

        with self.assertRaisesRegex(ValueError, 'invalid literal for int'):
            FioParser.parse_string(xml)


class TestRaiffeisenParser(TestCase):

    encoding = 'windows-1250'

    row_template = OrderedDict([('transaction_date', '15.9.2020'),
                                ('other_date', '14.09.2020'),
                                ('requested_amount', '7,50'),
                                ('currency', 'CZK'),
                                ('real_amount', '3,14'),
                                ('timestamp', '15.9.2020 14:15:16'),
                                ('counter_account_no', '123456'),
                                ('counter_account_bank', '7890'),
                                ('own_account_no', '987654'),
                                ('own_account_bank', '3210'),
                                ('variable_symbol', '0101'),
                                ('constant_symbol', '308'),
                                ('description', 'Ať se Vám daří!'),
                                ('status', '2'),
                                ('name', 'Jupiter Mining Company'),
                                ('identifier', '77777')])

    def _make_row(self, custom_data: Optional[Mapping[str, str]] = None) -> str:
        row = OrderedDict([(k, v) for k, v in self.row_template.items()])
        if custom_data:
            row.update(custom_data)
        return ';'.join(row.values()) + '\n'

    def _make_payment(self, row: Mapping[str, str]) -> Payment:
        counter_account = '{}/{}'.format(row['counter_account_no'], row['counter_account_bank'])

        payment = Payment()
        payment.identifier = row['identifier']
        payment.transaction_date = datetime.strptime(row['transaction_date'], '%d.%m.%Y').date()
        payment.counter_account = counter_account
        payment.name = row['name']
        payment.amount = Money(row['real_amount'].replace(',', '.'), 'CZK')
        payment.description = row['description']
        payment.constant_symbol = row['constant_symbol']
        payment.variable_symbol = row['variable_symbol']
        payment.specific_symbol = ''

        return payment

    def test_parse_string(self):
        statement = RaiffeisenParser.parse_string(self._make_row())

        self.assertEqual(statement.account_number, '987654/3210')
        self.assertEqual(statement.payments, [self._make_payment(self.row_template)])

    def test_parse_io(self):
        statement = RaiffeisenParser.parse_file(BytesIO(self._make_row().encode(self.encoding)))

        self.assertEqual(statement.account_number, '987654/3210')
        self.assertEqual(statement.payments, [self._make_payment(self.row_template)])

    def test_parse_file(self):
        with NamedTemporaryFile('bw') as f:
            f.write(self._make_row().encode(self.encoding))
            f.flush()
            statement = RaiffeisenParser.parse_file(f.name)

        self.assertEqual(statement.account_number, '987654/3210')
        self.assertEqual(statement.payments, [self._make_payment(self.row_template)])

    def test_row_length(self):
        row_short = self._make_row().split(';', 1)[1]
        with self.assertRaisesRegex(ValueError, 'Invalid number of columns in the CSV'):
            RaiffeisenParser.parse_string(row_short)

        row_long = self._make_row() + ';excessive data'
        with self.assertRaisesRegex(ValueError, 'Invalid number of columns in the CSV'):
            RaiffeisenParser.parse_string(row_long)

    def test_parse_non_czk_amount(self):
        source = self._make_row(custom_data={'real_amount': '3,14', 'currency': 'GBP'})
        statement = RaiffeisenParser.parse_string(source)
        self.assertEqual(statement.payments[0].amount, None)

    def test_parse_multiple_payments(self):
        source = self._make_row(custom_data={'identifier': '123'})
        source += self._make_row(custom_data={'identifier': '456'})
        statement = RaiffeisenParser.parse_string(source)

        payments = statement.payments
        self.assertEqual(len(payments), 2)
        self.assertEqual([p.identifier for p in payments], ['123', '456'])

    def test_error_on_multiple_statements(self):
        source = self._make_row(custom_data={'own_account_no': '1234567'})
        source += self._make_row(custom_data={'own_account_no': '2345678'})

        with self.assertRaisesRegex(ValueError, 'More than one account found in the statement'):
            RaiffeisenParser.parse_string(source)

    def test_check_status(self):
        source = self._make_row()
        source += self._make_row(custom_data={'identifier': '000', 'status': '7'})
        statement = RaiffeisenParser.parse_string(source)

        payments = statement.payments
        self.assertEqual(len(payments), 1)
        self.assertEqual(payments[0].identifier, self.row_template['identifier'])

    def test_parse_empty_string(self):
        '''When no payment suits the filter criteria the bank returns a file consisting of one space.'''
        source = ' '
        statement = RaiffeisenParser.parse_string(source)

        self.assertEqual(statement.account_number, None)
        self.assertEqual(len(statement.payments), 0)

    def test_parse_empty_counter_account(self):
        for field in ['counter_account_no', 'counter_account_bank']:
            with self.subTest(field=field):
                source = self._make_row(custom_data={field: ' '})
                statement = RaiffeisenParser.parse_string(source)
                self.assertEqual(statement.payments[0].counter_account, None)


class TestCSOBParser(TestCase):

    encoding = 'windows-1250'
    path = Path('teller/tests/data/example_csob.xml')

    def test_parse_file(self):
        statement = CSOBParser.parse_file(self.path)
        self.assertEqual(statement.account_number, '123456/7890')

        payments = statement.payments
        self.assertEqual(len(payments), 2)
        self.assertEqual(sum(p.amount for p in payments), Money(10.14, 'CZK'))

        payment = payments[0]
        self.assertEqual(payment.identifier, '776')
        self.assertEqual(payment.transaction_date, date(2020, 9, 20))
        self.assertEqual(payment.counter_account, '23456789/1111')
        self.assertEqual(payment.name, 'Společenství vlastníků jednotek')
        self.assertEqual(payment.amount, Money(3.14, 'CZK'))
        self.assertEqual(payment.description, 'CZNIC')
        self.assertEqual(payment.constant_symbol, '0308')
        self.assertEqual(payment.variable_symbol, '123')
        self.assertEqual(payment.specific_symbol, '456')

    def test_parse_string(self):
        with open(str(self.path), encoding=self.encoding) as f:
            content = f.read()
        statement = CSOBParser.parse_string(content)
        self.assertEqual(statement.account_number, '123456/7890')

        payments = statement.payments
        self.assertEqual(len(payments), 2)
        self.assertEqual(sum(p.amount for p in payments), Money(10.14, 'CZK'))

        payment = payments[0]
        self.assertEqual(payment.identifier, '776')
        self.assertEqual(payment.transaction_date, date(2020, 9, 20))
        self.assertEqual(payment.counter_account, '23456789/1111')
        self.assertEqual(payment.name, 'Společenství vlastníků jednotek')
        self.assertEqual(payment.amount, Money(3.14, 'CZK'))
        self.assertEqual(payment.description, 'CZNIC')
        self.assertEqual(payment.constant_symbol, '0308')
        self.assertEqual(payment.variable_symbol, '123')
        self.assertEqual(payment.specific_symbol, '456')

    def test_no_transactions(self):
        xml = '<FINSTA><FINSTA03><S25_CISLO_UCTU>123456/7890</S25_CISLO_UCTU>'
        xml += '</FINSTA03></FINSTA>'

        statement = CSOBParser.parse_string(xml)
        self.assertEqual(len(statement.payments), 0)

    def test_ignore_payments_by_code(self):
        xml = '<FINSTA><FINSTA03><S25_CISLO_UCTU>123456/7890</S25_CISLO_UCTU>'
        xml += '<FINSTA05><S28_POR_CISLO>1</S28_POR_CISLO><S61_CD_INDIK>C</S61_CD_INDIK></FINSTA05>'
        xml += '<FINSTA05><S28_POR_CISLO>2</S28_POR_CISLO><S61_CD_INDIK>CR</S61_CD_INDIK></FINSTA05>'
        xml += '</FINSTA03></FINSTA>'

        statement = CSOBParser.parse_string(xml)
        self.assertEqual(len(statement.payments), 1)
        self.assertEqual(statement.payments[0].identifier, '1')

    def test_invalid_code(self):
        xml = '<FINSTA><FINSTA03><S25_CISLO_UCTU>123456/7890</S25_CISLO_UCTU>'
        xml += '<FINSTA05><S28_POR_CISLO>1</S28_POR_CISLO><S61_CD_INDIK>X</S61_CD_INDIK></FINSTA05>'
        xml += '</FINSTA03></FINSTA>'

        with self.assertRaisesRegex(ValueError, 'Unknown payment code'):
            CSOBParser.parse_string(xml)

    def test_parse_incomplete_account_number(self):
        xml = '<FINSTA><FINSTA03><S25_CISLO_UCTU>123456/7890</S25_CISLO_UCTU>'
        xml += '<FINSTA05><S61_CD_INDIK>C</S61_CD_INDIK>'
        xml += '<PART_ACCNO>1234567</PART_ACCNO>'
        xml += '<PART_BANK_ID></PART_BANK_ID>'
        xml += '</FINSTA05>'
        xml += '</FINSTA03></FINSTA>'

        statement = CSOBParser.parse_string(xml)
        self.assertEqual(statement.payments[0].counter_account, None)
