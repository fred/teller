#
# Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from datetime import date
from typing import Any, Dict
from unittest import TestCase

from moneyed import Money

from teller.statement import BankStatement, Payment


class TestBankStatement(TestCase):

    def test_account_number(self):
        statement = BankStatement('123456/7890')
        self.assertEqual('123456/7890', statement.account_number)

    def test_add_payment(self):
        statement = BankStatement('123456/7890')
        statement.add_payment(Payment(identifier='abc', amount=Money(1, 'GBP')))

        self.assertEqual(len(statement.payments), 1)
        self.assertEqual(statement.payments[0].identifier, 'abc')

    def test_repr(self):
        statement = BankStatement('123456/7890')
        self.assertEqual(str(statement), '<BankStatement: 123456/7890>')
        self.assertEqual(repr(BankStatement()), '<BankStatement: None>')

    def test_iter(self):
        payment_1 = Payment(identifier='abc', amount=Money(1, 'CZK'))
        payment_2 = Payment(identifier='xyz', amount=Money(1, 'GBP'))

        statement = BankStatement('123456/7890')
        statement.add_payment(payment_1)
        statement.add_payment(payment_2)

        self.assertEqual([p for p in statement], [payment_1, payment_2])


class TestPayment(TestCase):
    parameters: Dict[str, Any] = {'identifier': 'abc',
                                  'transaction_date': date(2020, 1, 15),
                                  'counter_account': '987654/3210',
                                  'name': 'John Doe',
                                  'amount': Money(3.14, 'GBP'),
                                  'description': 'Have a nice day.',
                                  'constant_symbol': '123',
                                  'variable_symbol': '456',
                                  'specific_symbol': '789'}

    def test_parameters(self):
        payment = Payment(**self.parameters)

        self.assertEqual(payment.identifier, 'abc')
        self.assertEqual(payment.transaction_date, date(2020, 1, 15))
        self.assertEqual(payment.counter_account, '987654/3210')
        self.assertEqual(payment.name, 'John Doe')
        self.assertEqual(payment.amount, Money(3.14, 'GBP'))
        self.assertEqual(payment.description, 'Have a nice day.')
        self.assertEqual(payment.constant_symbol, '123')
        self.assertEqual(payment.variable_symbol, '456')
        self.assertEqual(payment.specific_symbol, '789')

    def test_repr(self):
        payment = Payment(**self.parameters)
        self.assertEqual(repr(payment), '<Payment: abc>')
        self.assertEqual(repr(Payment()), '<Payment: None>')

    def test_eq(self):
        payment_1 = Payment(**self.parameters)
        self.assertEqual(payment_1, Payment(**self.parameters))

        for key in self.parameters:
            payment_2 = Payment(**self.parameters)
            payment_2.__setattr__(key, 'some_other_value')
            self.assertNotEqual(payment_1, payment_2)
