#
# Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
from datetime import datetime
from pathlib import Path
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel
from urllib.parse import urlencode

import responses
import zeep

from teller.downloaders import (BankStatementDownloader, CSOBStatementDownloader, FioStatementDownloader,
                                HttpStatementDownloader, RaiffeisenStatementDownloader, RawStatement,
                                TellerDownloadError)


class DummyStatementDownloader(BankStatementDownloader):
    def _download_data(self, start_date, end_date):
        pass  # pragma: no cover


class TestRawStatement(TestCase):

    def test_no_metadata(self):
        statement = RawStatement(b'Hello!')
        self.assertEqual(statement.content, b'Hello!')

    def test_metadata(self):
        statement = RawStatement(b'Hello!', name='file.txt', encoding='utf-8')
        self.assertEqual(statement.content, b'Hello!')
        self.assertEqual(statement.name, 'file.txt')
        self.assertEqual(statement.encoding, 'utf-8')

    def test_equal(self):
        self.assertNotEqual(RawStatement(b'a', name='a'), RawStatement(b'b', name='a'))
        self.assertNotEqual(RawStatement(b'a', name='a'), RawStatement(b'a', name='b'))
        self.assertNotEqual(RawStatement(b'a', encoding='utf-8'), RawStatement(b'a', encoding='windows-1250'))
        self.assertEqual(RawStatement(b'a'), RawStatement(b'a'))
        self.assertEqual(RawStatement(b'a', name='b', encoding='utf-8'), RawStatement(b'a', name='b', encoding='utf-8'))

    def test_repr(self):
        self.assertEqual(repr(RawStatement(b'Short')), "<RawStatement: b'Short'>")
        self.assertEqual(repr(RawStatement(b'Very long statement')), "<RawStatement: b'Very long st...'>")
        self.assertEqual(repr(RawStatement(b'Short', name='a')), "<RawStatement: b'Short', name: a>")
        self.assertEqual(repr(RawStatement(b'Short', encoding='a')), "<RawStatement: b'Short', encoding: a>")
        self.assertEqual(
            repr(RawStatement(b'Short', name='a', encoding='b')),
            "<RawStatement: b'Short', name: a, encoding: b>"
        )

    def test_buffer(self):
        statement = RawStatement(b'Statement!')
        self.assertEqual(statement.buffer.read(), b'Statement!')


class TestBankStatementDownloader(TestCase):

    def test_enforce_https(self):
        with self.assertWarnsRegex(UserWarning, 'Sensitive data is transferred'):
            DummyStatementDownloader('http://bank.test/', 1)

    def test_ends_with_slash(self):
        with self.assertWarnsRegex(UserWarning, "base_url should end with '/'"):
            DummyStatementDownloader('https://bank.test', 1)

    def test_check_arguments(self):
        downloader = DummyStatementDownloader('https://bank.test/', 1)
        with self.assertRaisesRegex(ValueError, 'Start date is greater than end date'):
            downloader.get_statements(start_date=datetime(2020, 10, 1), end_date=datetime(1999, 1, 1))


class TestHttpStatementDownloader(TestCase):
    url = 'https://bank.test/'
    password = 'letmein'
    start_date = datetime(2020, 9, 3)
    end_date = datetime(2020, 10, 2)

    def test_download_data(self):
        downloader = HttpStatementDownloader(self.url)

        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, self.url, body='It works!', status=200, content_type='text/plain;charset=utf-8')
            result = downloader.get_statements(self.start_date, self.end_date)
        self.assertEqual((RawStatement(b'It works!', encoding='utf-8'),), result)

    def test_statement_not_found(self):
        downloader = HttpStatementDownloader(self.url)

        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, self.url, body='Not found.', status=404, content_type='text/plain;charset=utf-8')
            with self.assertRaisesRegex(TellerDownloadError, 'Statement could not be retrieved'):
                downloader.get_statements(self.start_date, self.end_date)


class TestFioStatementDownloader(TestCase):
    url = 'https://bank.test/'
    password = 'letmein'
    start_date = datetime(2020, 9, 3)
    end_date = datetime(2020, 10, 2)

    def test_compose_url(self):
        downloader = FioStatementDownloader(self.url, self.password)

        result = downloader._compose_url(self.start_date, self.end_date)

        full_url = '{}ib_api/rest/periods/{}/2020-09-03/2020-10-02/transactions.xml'.format(self.url, self.password)
        self.assertEqual(result, full_url)

    def test_get_statements(self):
        downloader = FioStatementDownloader(self.url, self.password)
        statement = b'I am a fish.'

        full_url = '{}ib_api/rest/periods/{}/2020-09-03/2020-10-02/transactions.xml'.format(self.url, self.password)
        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, full_url, body=statement, status=200, content_type='text/plain;charset=utf-8')
            result = downloader.get_statements(self.start_date, self.end_date)

        self.assertEqual(result, (RawStatement(statement, encoding='utf-8'),))


class TestRaiffeisenStatementDownloader(TestCase):
    url = 'https://bank.test/'
    password = 'letmein'
    shop = 'Name'
    account = 123
    bank = 1001
    start_date = datetime(2020, 9, 3)
    end_date = datetime(2020, 10, 2)

    def test_compose_url(self):
        downloader = RaiffeisenStatementDownloader(self.url, self.shop, self.account, self.bank, self.password)

        result = downloader._compose_url(self.start_date, self.end_date)

        full_url = '{}ibs/eshop/payments-list'.format(self.url)
        self.assertEqual(result, full_url)

    def test_get_statements(self):
        downloader = RaiffeisenStatementDownloader(self.url, self.shop, self.account, self.bank, self.password)
        statement = b'I am a fish.'

        params = {'shopname': self.shop,
                  'creditaccount': str(self.account),
                  'creditbank': str(self.bank),
                  'password': self.password,
                  'listtype': 'PLAIN',
                  'paidfrom': '03.09.2020',
                  'paidto': '02.10.2020'}
        full_url = '{}ibs/eshop/payments-list?{}'.format(self.url, urlencode(params))
        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, full_url, body=statement, status=200, content_type='text/plain;charset=utf-8')
            result = downloader.get_statements(self.start_date, self.end_date)

        self.assertEqual(result, (RawStatement(statement + b'\n', encoding='utf-8'),))

    def test_empty_statements(self):
        # When no payment suits the filter criteria the bank returns a file consisting of one space.
        downloader = RaiffeisenStatementDownloader(self.url, self.shop, self.account, self.bank, self.password)

        url = '{}ibs/eshop/payments-list'.format(self.url)
        with responses.RequestsMock() as rsps:
            content_type = 'text/plain;charset=utf-8'
            rsps.add(responses.GET, url, body=' ', status=200, content_type=content_type, match_querystring=False)
            rsps.add(responses.GET, url, body='\n', status=200, content_type=content_type, match_querystring=False)
            rsps.add(responses.GET, url, body=' \n', status=200, content_type=content_type, match_querystring=False)
            rsps.add(responses.GET, url, body=' abc\n', status=200, content_type=content_type, match_querystring=False)
            rsps.add(
                responses.GET, url, body='ábč\n'.encode('cp1250'), status=200,
                content_type='text/plain;charset=cp1250', match_querystring=False,
            )
            result_1 = downloader.get_statements(self.start_date, self.end_date)
            result_2 = downloader.get_statements(self.start_date, self.end_date)
            result_3 = downloader.get_statements(self.start_date, self.end_date)
            result_4 = downloader.get_statements(self.start_date, self.end_date)
            result_5 = downloader.get_statements(self.start_date, self.end_date)

        self.assertEqual(result_1, (RawStatement(b'', encoding='utf-8'),))
        self.assertEqual(result_2, (RawStatement(b'', encoding='utf-8'),))
        self.assertEqual(result_3, (RawStatement(b'', encoding='utf-8'),))
        self.assertEqual(result_4, (RawStatement(b' abc\n', encoding='utf-8'),))
        self.assertEqual(result_5, (RawStatement('ábč\n'.encode('cp1250'), encoding='cp1250'),))

    def test_get_statements_filter_account(self):
        # Test records which belongs to another account are filtered out of the statement.
        downloader = RaiffeisenStatementDownloader(self.url, self.shop, self.account, self.bank, self.password)
        body = ';;;;;;;;123;1001;;;;;;1\n;;;;;;;;42-123;1001;;;;;;2\n;;;;;;;;123;1001;;;;;;3\n'

        url = '{}ibs/eshop/payments-list'.format(self.url)
        with responses.RequestsMock() as rsps:
            content_type = 'text/plain;charset=utf-8'
            rsps.add(responses.GET, url, body=body, status=200, content_type=content_type, match_querystring=False)
            result = downloader.get_statements(self.start_date, self.end_date)

        content = b';;;;;;;;123;1001;;;;;;1\n;;;;;;;;123;1001;;;;;;3\n'
        self.assertEqual(result, (RawStatement(content, encoding='utf-8'),))


class TestCSOBStatementDownloader(TestCase):
    url = 'https://bank.test/cebbc/api/'
    wsdl = 'teller/tests/data/cebbc-wsdl/CEBBCWS.wsdl'
    contract = '12345'
    start_date = datetime(2020, 9, 3)
    end_date = datetime(2020, 10, 2)
    cert = Path('path/to/cert')
    key = Path('path/to/key')

    FileDetail = zeep.Client(wsdl).type_factory('http://ceb-bc.csob.cz/CEBBCWS/GetDownloadFileList_v3').FileDetail_v2

    def test_statement_frequency_for_gpc_only(self):
        with self.assertRaisesRegex(ValueError, 'Parameter "statement_frequency" only alowed for GPC statements'):
            CSOBStatementDownloader(
                self.url,
                self.wsdl,
                file_type=CSOBStatementDownloader.FileType.XML,
                statement_frequency=CSOBStatementDownloader.StatementFrequency.DAILY
            )

    def test_get_statements(self):
        downloader = CSOBStatementDownloader(self.url, self.wsdl)
        with patch.object(downloader, '_get_statement_details', autospec=True) as mock_method:
            mock_method.return_value = iter([
                (self.url + 'file_1', 'file_1.txt', True),
                (self.url + 'file_2', 'file_2.txt', True),
            ])
            with responses.RequestsMock() as rsps:
                content_type = 'text/plain;charset=utf-8'
                rsps.add(responses.GET, self.url + 'file_1', body='<content_1>', status=200, content_type=content_type)
                rsps.add(responses.GET, self.url + 'file_2', body='<content_2>', status=200, content_type=content_type)
                result = downloader.get_statements(self.start_date, self.end_date)
        expected = (
            RawStatement(b'<content_1>', name='file_1.txt', encoding='utf-8'),
            RawStatement(b'<content_2>', name='file_2.txt', encoding='utf-8'),
        )
        self.assertEqual(result, expected)

    @patch('teller.downloaders.sleep', autospec=True)
    def test_get_statements_not_ready(self, mock_sleep):
        downloader = CSOBStatementDownloader(self.url, self.wsdl, retries=3)
        with patch.object(downloader, '_get_statement_details', autospec=True) as mock_method:
            return_value = [
                (self.url + 'file_1', 'file_1.txt', True),
                (self.url + 'file_2', 'file_2.txt', False),
            ]
            # We use lambda to recycle the iterator so we get results for retries as well.
            mock_method.side_effect = lambda *args, **kwargs: iter(return_value)
            with responses.RequestsMock() as rsps:
                rsps.add(
                    responses.GET,
                    self.url + 'file_1',
                    body='<content_1>',
                    status=200,
                    content_type='text/plain;charset=utf-8'
                )
                with self.assertLogs() as log:
                    result = downloader.get_statements(self.start_date, self.end_date)

        expected_logs = [
            'INFO:teller.downloaders:Not all statements ready. Retry downloading statement details.',
            'INFO:teller.downloaders:Not all statements ready. Retry downloading statement details.',
            'INFO:teller.downloaders:Not all statements ready. Retry downloading statement details.',
            'WARNING:teller.downloaders:Could not get file file_2.txt - file not ready.',
        ]
        self.assertEqual(log.output, expected_logs)
        mock_sleep.assert_has_calls(3 * [call(3 * 60)])

        expected = (RawStatement(b'<content_1>', name='file_1.txt', encoding='utf-8'),)
        self.assertEqual(result, expected)

    @patch('teller.downloaders.sleep', autospec=True)
    def test_get_statements_ready_after_retry(self, mock_sleep):
        downloader = CSOBStatementDownloader(self.url, self.wsdl, retries=3)
        with patch.object(downloader, '_get_statement_details', autospec=True) as mock_method:
            mock_method.side_effect = [
                iter([(self.url + 'file_1', 'file_1.txt', True), (self.url + 'file_2', 'file_2.txt', False)]),
                iter([(self.url + 'file_1', 'file_1.txt', True), (self.url + 'file_2', 'file_2.txt', True)]),
            ]
            with responses.RequestsMock() as rsps:
                content_type = 'text/plain;charset=utf-8'
                rsps.add(responses.GET, self.url + 'file_1', body='<content_1>', status=200, content_type=content_type)
                rsps.add(responses.GET, self.url + 'file_2', body='<content_2>', status=200, content_type=content_type)
                with self.assertLogs() as log:
                    result = downloader.get_statements(self.start_date, self.end_date)

        expected_logs = ['INFO:teller.downloaders:Not all statements ready. Retry downloading statement details.']
        self.assertEqual(log.output, expected_logs)
        mock_sleep.assert_has_calls([call(3 * 60)])

        expected = (
            RawStatement(b'<content_1>', name='file_1.txt', encoding='utf-8'),
            RawStatement(b'<content_2>', name='file_2.txt', encoding='utf-8'))
        self.assertEqual(result, expected)

    def test_get_statements_missing(self):
        downloader = CSOBStatementDownloader(self.url, self.wsdl)
        with patch.object(downloader, '_get_statement_details', autospec=True) as mock_method:
            mock_method.return_value = iter([(self.url + 'file_1', 'file_1.txt', True)])
            with responses.RequestsMock() as rsps:
                rsps.add(responses.GET, self.url + 'file_1', body='Nothing', status=404, content_type='text/plain')
                expected_message = 'Statement at https://bank.test/cebbc/api/file_1 could not be obtained'
                with self.assertRaisesRegex(TellerDownloadError, expected_message):
                    list(downloader.get_statements(self.start_date, self.end_date))

    def test_download_data_certs(self):
        downloader = CSOBStatementDownloader(self.url, self.wsdl, cert_path=self.cert, key_path=self.key)

        with patch.object(downloader, '_get_statement_details', autospec=True) as get_statement_details:
            get_statement_details.return_value = iter([('url_1.test', 'file_1.txt', True)])
            with patch('requests.get') as requests_get:
                requests_get.return_value.ok = True
                requests_get.return_value.content = b'content'
                list(downloader.get_statements(self.start_date, self.end_date))
        requests_get.assert_called_with('url_1.test', cert=(str(self.cert), str(self.key)), timeout=3)

    @patch('zeep.Client', autospec=True)
    def test_create_service(self, mock_client):
        downloader = CSOBStatementDownloader(self.url, self.wsdl, key_path=self.key, cert_path=self.cert)

        mock_client.return_value.create_service.return_value = sentinel.service

        result = downloader._create_service()

        self.assertEqual(sentinel.service, result)
        self.assertEqual(str(self.wsdl), mock_client.call_args[0][0])
        self.assertEqual((str(self.cert), str(self.key)), mock_client.call_args[1]['transport'].session.cert)

        mock_client.return_value.create_service.assert_called_with('{http://ceb-bc.csob.cz/CEBBCWS}CEBBCWSBinding',
                                                                   self.url)

    def test_get_statement_details(self):
        file_list = [
            self.FileDetail(Type='VYPIS', Status='D', Filename='file_1.xml', Url='https://bank.test/file_1'),
            self.FileDetail(Type='VYPIS', Status='D', Filename='file_2.xml', Url='https://bank.test/file_2'),
            self.FileDetail(Type='VYPIS', Status='R', Filename='file_4.xml', Url='https://bank.test/file_4'),
        ]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract)
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        filter_criteria = {
            'FileTypes': ['VYPIS'],
            'FileFormats': ['XML'],
            'CreatedAfter': self.start_date,
            'CreatedBefore': self.end_date,
        }
        service_mock.GetDownloadFileList_v3.assert_called_with(self.contract, Filter=filter_criteria)

        expected_result = [
            ('https://bank.test/file_1', 'file_1.xml', True),
            ('https://bank.test/file_2', 'file_2.xml', True),
            ('https://bank.test/file_4', 'file_4.xml', False),
        ]
        self.assertEqual(result, expected_result)

    def test_get_statement_details_no_match(self):
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList = None

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract)
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        self.assertEqual(result, [])

    def test_get_statement_details_frequency(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='D', Filename='123456789_20200904_D_10_CZ.GPC',
                                     Url='https://bank.test/file_1'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='123456789_20200904_M_10_CZ.GPC',
                                     Url='https://bank.test/file_2')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract,
                                             file_type=CSOBStatementDownloader.FileType.GPC,
                                             statement_frequency=CSOBStatementDownloader.StatementFrequency.DAILY)
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        self.assertEqual([('https://bank.test/file_1', '123456789_20200904_D_10_CZ.GPC', True)], result)

    def test_get_statement_details_service_error(self):
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.side_effect = Exception

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract)
        with patch.object(downloader, '_create_service', autospec=True) as create_service:
            create_service.return_value = service_mock
            with self.assertRaisesRegex(TellerDownloadError, 'Could not obtain file list'):
                list(downloader._get_statement_details(self.start_date, self.end_date))

    def test_get_statement_details_file_error(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='F', Filename='file_1.xml', Url='https://bank.test/file_1')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract)
        with patch.object(downloader, '_create_service', autospec=True) as create_service:
            create_service.return_value = service_mock
            with self.assertRaisesRegex(TellerDownloadError, 'Could not get file file_1.xml - error on the bank side'):
                list(downloader._get_statement_details(self.start_date, self.end_date))

    def test_get_statement_details_unknown_status_error(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='X', Filename='file_1.xml', Url='https://bank.test/file_1')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract)
        with patch.object(downloader, '_create_service', autospec=True) as create_service:
            create_service.return_value = service_mock
            with self.assertRaisesRegex(TellerDownloadError, 'Unknown status of file file_1.xml'):
                list(downloader._get_statement_details(self.start_date, self.end_date))

    def test_include_accounts(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='D', Filename='11111_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_1'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='22222_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_2')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract,
                                             include_accounts=['11111'])
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        expected_result = [('https://bank.test/file_1', '11111_20210102_1_CZ.xml', True)]
        self.assertEqual(result, expected_result)

    def test_exclude_accounts(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='D', Filename='11111_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_1'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='22222_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_2')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract,
                                             exclude_accounts=['22222'])
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        expected_result = [('https://bank.test/file_1', '11111_20210102_1_CZ.xml', True)]
        self.assertEqual(result, expected_result)

    def test_exclude_accounts_can_not_be_str(self):
        message = 'Parameter exclude_accounts should be a list of strings not a single string'
        with self.assertRaisesRegex(ValueError, message):
            CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract, exclude_accounts='22222')

    def test_include_accounts_can_not_be_str(self):
        message = 'Parameter include_accounts should be a list of strings not a single string'
        with self.assertRaisesRegex(ValueError, message):
            CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract, include_accounts='22222')

    def test_include_exclude_account(self):
        file_list = [self.FileDetail(Type='VYPIS', Status='D', Filename='11111_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_1'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='22222_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_2'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='33333_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_3'),
                     self.FileDetail(Type='VYPIS', Status='D', Filename='44444_20210102_1_CZ.xml',
                                     Url='https://bank.test/file_4')]
        service_mock = Mock()
        service_mock.GetDownloadFileList_v3.return_value.FileList.FileDetail = file_list

        downloader = CSOBStatementDownloader(self.url, self.wsdl, contract_number=self.contract,
                                             include_accounts=['11111', '22222'], exclude_accounts=['22222', '33333'])
        with patch.object(downloader, '_create_service', return_value=service_mock, autospec=True):
            result = list(downloader._get_statement_details(self.start_date, self.end_date))

        expected_result = [('https://bank.test/file_1', '11111_20210102_1_CZ.xml', True)]
        self.assertEqual(result, expected_result)
