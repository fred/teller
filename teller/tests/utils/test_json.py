#
# Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

import json
from datetime import date
from operator import attrgetter
from unittest import TestCase

from moneyed import Money

from teller.statement import BankStatement, Payment
from teller.utils.json import StatementJSONDecoder, StatementJSONEncoder


class TestBankStatementEncoderAndDecoder(TestCase):

    def test_encode_decode_statement(self):
        original = BankStatement('123456/7890')
        original.add_payment(Payment(identifier='abc', amount=Money(1, 'CZK')))
        original.add_payment(Payment(identifier='efg', amount=Money(2, 'CZK')))

        encoded = json.dumps(original, cls=StatementJSONEncoder)
        decoded = StatementJSONDecoder.loads(encoded)

        self.assertTrue(isinstance(decoded, BankStatement))
        self.assertEqual(original.account_number, decoded.account_number)  # type: ignore

        self.assertEqual(sorted(original.payments, key=attrgetter('identifier')),
                         sorted(decoded.payments, key=attrgetter('identifier')))  # type: ignore

    def test_encode_decode_payment(self):
        original_payment = Payment(identifier='abc',
                                   transaction_date=date(2020, 1, 15),
                                   counter_account='987654/3210',
                                   name='John Doe',
                                   amount=Money(3.14, 'GBP'),
                                   description='Have a nice day.',
                                   constant_symbol='123',
                                   variable_symbol='456',
                                   specific_symbol='789')
        original_statement = BankStatement('123456/7890')
        original_statement.add_payment(original_payment)

        encoded = json.dumps(original_statement, cls=StatementJSONEncoder)
        decoded = StatementJSONDecoder.loads(encoded)

        self.assertTrue(isinstance(decoded, BankStatement))
        self.assertEqual(original_statement.payments, decoded.payments)  # type: ignore

    def test_invalid_statement_fields(self):
        invalid = '{"unknown": "abc"}'
        decoded = StatementJSONDecoder.loads(invalid)
        self.assertEqual({"unknown": "abc"}, decoded)

    def test_invalid_payment_fields(self):
        invalid = '{"account_number": "123456/7890", "payments": [{"invalid": "abc"}]}'
        decoded = StatementJSONDecoder.loads(invalid)
        expected = {"account_number": "123456/7890", "payments": [{"invalid": "abc"}]}
        self.assertEqual(expected, decoded)

    def test_payments_not_iterable(self):
        invalid = '{"account_number": "123456/7890", "payments": 123}'
        decoded = StatementJSONDecoder.loads(invalid)
        expected = {"account_number": "123456/7890", "payments": 123}
        self.assertEqual(expected, decoded)

    def test_raises_correct_error_when_serialization_fails(self):
        with self.assertRaisesRegex(TypeError, 'is not JSON serializable'):
            json.dumps(complex(), cls=StatementJSONEncoder)
