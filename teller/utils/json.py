#
# Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
"""A module providing JSON utilities to (de)serialize BankStatement and Payment."""

from datetime import date, datetime
from json import JSONEncoder, loads
from typing import Any, Dict, Mapping, Optional, Union

from moneyed import Money

from teller.statement import BankStatement, Payment


class StatementJSONEncoder(JSONEncoder):
    """JSONEncoder for serializing BankStatements."""

    date_format = '%Y-%m-%d'

    def default(self, obj):
        """Serialize Payment and Statement instance to JSON."""
        if isinstance(obj, BankStatement):
            data: Dict[str, Any] = {'account_number': obj.account_number,
                                    'payments': obj.payments}
            return data
        elif isinstance(obj, Payment):
            data = {'identifier': obj.identifier,
                    'transaction_date': self._convert_date(obj.transaction_date),
                    'counter_account': obj.counter_account,
                    'name': obj.name,
                    'amount': obj.amount,
                    'description': obj.description,
                    'constant_symbol': obj.constant_symbol,
                    'variable_symbol': obj.variable_symbol,
                    'specific_symbol': obj.specific_symbol}
            return data
        elif isinstance(obj, Money):
            return (str(obj.amount), str(obj.currency))
        else:
            return super().default(obj)

    def _convert_date(self, date: Optional[date]) -> Optional[str]:
        if date is not None:
            return date.strftime(self.date_format)
        else:
            return None


class StatementJSONDecoder():
    """A class to decode JSON serialized Statement objects."""

    date_format = '%Y-%m-%d'

    @classmethod
    def loads(cls, source: str) -> Union[BankStatement, Mapping]:
        """Deserialize Statement from JSON string."""
        raw = loads(source)
        try:
            return cls._convert_to_statement(raw)
        except TypeError:
            return raw

    @classmethod
    def _convert_to_statement(cls, raw: Mapping[str, Any]) -> BankStatement:
        """Deserialize the statement from JSON string."""
        if set(raw.keys()) != set(('account_number', 'payments')):
            raise TypeError('Incompatible fields.')

        statement = BankStatement(raw.get('account_number'))

        for payment in raw.get('payments', []):
            statement.add_payment(cls._convert_to_payment(payment))
        return statement

    @classmethod
    def _convert_to_payment(cls, raw: Mapping[str, Any]) -> Payment:
        converted = dict(**raw)

        if converted.get('transaction_date', None):
            transaction_date = converted['transaction_date']
            converted['transaction_date'] = datetime.strptime(transaction_date, cls.date_format).date()
        if converted.get('amount', None):
            amount, currency = converted['amount']
            converted['amount'] = Money(amount, currency)

        return Payment(**converted)
