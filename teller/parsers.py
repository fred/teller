#
# Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""A module providing parsers for bank statements."""
import csv
from abc import ABC, abstractmethod
from datetime import date, datetime
from enum import IntEnum, unique
from io import BytesIO, TextIOWrapper
from pathlib import Path
from typing import BinaryIO, Iterable, List, Optional, Set, TextIO, Tuple, Union
from xml.etree.ElementTree import Element  # nosec - for typing only, for parsing use defusedxml, see below

# see: https://docs.python.org/3/library/xml.html#xml-vulnerabilities
from defusedxml.ElementTree import XMLParser, parse
from moneyed import Money

from teller.statement import BankStatement, Payment

SourceType = Union[str, Path, BinaryIO]


class BankStatementParser(ABC):
    """Base class for Statement parsers."""

    @classmethod
    def parse_string(cls, source: str) -> BankStatement:
        """Parse Bank statement from a string.

        Args:
            source: The string containing the statement to be parsed.
        """
        encoding = 'utf-8'
        stream = BytesIO(source.encode(encoding))
        return cls.parse_file(stream, encoding=encoding)

    @classmethod
    @abstractmethod
    def parse_file(cls, source: SourceType, encoding=None) -> BankStatement:
        """Parse Bank statement from a file.

        Args:
            source: Either a path to the file or an io object with the data.
            encoding: Setting this parameter enforces encoding to the parser. Otherwise it tries to guess it from
                the source or uses the default.
        """
        raise NotImplementedError('This is abstract base class use bank specific parser insted.')


class FioParser(BankStatementParser):
    """Parser for Fio bank statements."""

    date_format = '%Y-%m-%d%z'

    @classmethod
    def parse_file(cls, source: SourceType, encoding=None) -> BankStatement:
        """Parse Bank statement from a file.

        Args:
            source: Either a path to the file or an io object with the data.
            encoding: Setting this parameter enforces encoding to the parser. Otherwise it tries to guess it from
                the source or uses the default.
        """
        # parse() can not handle Path type in python 3.5
        if isinstance(source, Path):
            source = str(source)

        tree = parse(source, XMLParser(encoding=encoding))
        header = tree.find('Info')

        if header is None:
            raise ValueError('Statement is missing header.')

        transaction_list = tree.find('TransactionList')

        account_number = FioParser._parse_account(header)
        statement = BankStatement(account_number=account_number)

        if transaction_list is not None:
            for transaction in transaction_list:
                statement.add_payment(FioParser._parse_transaction(transaction))

        return statement

    @staticmethod
    def _parse_account(header: Element) -> str:
        account_id = header.findtext('accountId')
        bank_id = header.findtext('bankId')
        if account_id and bank_id:
            return '{}/{}'.format(account_id, bank_id)
        else:
            raise ValueError('Could not parse account number.')

    @staticmethod
    def _parse_transaction(element: Element) -> Payment:
        payment = Payment()

        payment.identifier = element.findtext('column_22', None)
        payment.transaction_date = FioParser._parse_date(element)
        payment.counter_account = FioParser._parse_counter_account(element)
        payment.name = element.findtext('column_10', None)
        payment.amount = FioParser._parse_amount(element)
        payment.description = element.findtext('column_16', None)
        payment.constant_symbol = element.findtext('column_4', None)
        payment.variable_symbol = element.findtext('column_5', None)
        payment.specific_symbol = element.findtext('column_6', None)

        return payment

    @staticmethod
    def _parse_amount(element: Element) -> Optional[Money]:
        amount = element.findtext('column_1', None)
        currency = element.findtext('column_14', None)
        if amount is not None and currency is not None:
            return Money(amount, currency)
        else:
            return None

    @staticmethod
    def _parse_date(element: Element) -> Optional[date]:
        date_str = element.findtext('column_0', None)
        if date_str:
            # the date is provided with timezone (e.g. 2012-12-21+01:00) which we ignore
            # also python 3.5 can not parse date
            year = int(date_str[0:4])
            month = int(date_str[5:7])
            day = int(date_str[8:10])
            return date(year, month, day)
        else:
            return None

    @staticmethod
    def _parse_counter_account(element: Element) -> Optional[str]:
        account_number = element.findtext('column_2', None)
        bank_id = element.findtext('column_3', None)

        # Sometimes column_3 is missing and column_26 for BIC is present - use it instead of the bank code
        if not bank_id:
            bank_id = element.findtext('column_26', None)

        if account_number and bank_id:
            return account_number + '/' + bank_id
        else:
            return None


class RaiffeisenParser(BankStatementParser):
    """Parser for Raiffeisen bank statements."""

    row_length = 16
    date_format = '%d.%m.%Y'
    STATUS_OK = '2'

    @unique
    class Columns(IntEnum):
        TRANSACTION_DATE = 0
        CURRENCY = 3
        AMOUNT = 4
        COUNTER_ACCOUNT_NO = 6
        COUNTER_ACCOUNT_BANK = 7
        OWN_ACCOUNT_NO = 8
        OWN_ACCOUNT_BANK = 9
        VARIABLE_SYMBOL = 10
        CONSTANT_SYMBOL = 11
        DESCRIPTION = 12
        STATUS = 13
        NAME = 14
        IDENTIFIER = 15

    @classmethod
    def parse_file(cls, source: SourceType, encoding='windows-1250') -> BankStatement:
        """Parse Bank statement from a file.

        Args:
            source: Either a path to the file or an io object with the data.
            encoding: Setting this parameter enforces encoding to the parser. Otherwise it uses default.
        """
        if isinstance(source, str) or isinstance(source, Path):
            # Python 3.5 can not handle Path
            source_str = str(source)
            with open(source_str, mode='tr', encoding=encoding) as f:
                return cls._parse_io(f)
        else:
            text = TextIOWrapper(source, encoding=encoding)
            return cls._parse_io(text)

    @classmethod
    def _parse_io(cls, source: TextIO) -> BankStatement:
        payments = []
        own_accounts = set()
        reader = csv.reader(source, delimiter=';')
        for row in reader:
            if len(row) == 1 and row[0] == ' ':
                # When no payment suits the filter criteria the bank returns a file consisting of one space.
                continue
            if len(row) != cls.row_length:
                raise ValueError('Invalid number of columns in the CSV.')
            if cls._check_status_ok(row):
                own_account, payment = cls._process_row(row)
                payments.append(payment)
                own_accounts.add(own_account)
        return cls._make_statement(own_accounts, payments)

    @classmethod
    def _process_row(cls, row: List[str]) -> Tuple[Optional[str], Payment]:
        row = [item.strip() for item in row]

        own_account = cls._parse_account(row[cls.Columns.OWN_ACCOUNT_NO], row[cls.Columns.OWN_ACCOUNT_BANK])

        payment = Payment()
        payment.identifier = row[cls.Columns.IDENTIFIER]
        payment.transaction_date = datetime.strptime(row[cls.Columns.TRANSACTION_DATE], cls.date_format).date()
        payment.counter_account = cls._parse_account(row[cls.Columns.COUNTER_ACCOUNT_NO],
                                                     row[cls.Columns.COUNTER_ACCOUNT_BANK])
        payment.name = row[cls.Columns.NAME]
        payment.amount = cls._parse_amount(row[cls.Columns.AMOUNT], row[cls.Columns.CURRENCY])
        payment.description = row[cls.Columns.DESCRIPTION]
        payment.constant_symbol = row[cls.Columns.CONSTANT_SYMBOL]
        payment.variable_symbol = row[cls.Columns.VARIABLE_SYMBOL]
        payment.specific_symbol = ''

        return own_account, payment

    @classmethod
    def _check_status_ok(cls, row: List[str]) -> bool:
        status = row[cls.Columns.STATUS].strip()
        return status == cls.STATUS_OK

    @staticmethod
    def _parse_account(account_number: str, bank_id: str) -> Optional[str]:
        account_number = account_number.strip()
        bank_id = bank_id.strip()
        if account_number and bank_id:
            return '{}/{}'.format(account_number, bank_id)
        else:
            return None

    @staticmethod
    def _parse_amount(amount: str, currency: str) -> Optional[Money]:
        if currency == 'CZK':
            amount = amount.replace(',', '.').replace(' ', '')
            return Money(amount, currency)
        else:
            # Transfers which are not in CZK are processed throught raiffeisen TXT reports we set price to None here
            # in order to import the payment and be sure that it will not be processed by backend till correct price
            # in CZK is known.
            return None

    @staticmethod
    def _make_statement(own_accounts: Set[Optional[str]], payments: Iterable[Payment]) -> BankStatement:
        if len(own_accounts) == 0:
            own_account = None
        elif len(own_accounts) == 1:
            own_account = next(iter(own_accounts))
        else:
            raise ValueError('More than one account found in the statement.')

        statement = BankStatement(own_account)
        for payment in payments:
            statement.add_payment(payment)
        return statement


class CSOBParser(BankStatementParser):
    """Parser for CSOB bank statements."""

    date_format = '%d.%m.%Y'

    @classmethod
    def parse_file(cls, source: SourceType, encoding=None) -> BankStatement:
        """Parse Bank statement from a file.

        Args:
            source: Either a path to the file or an io object with the data.
            encoding: Setting this parameter enforces encoding to the parser. Otherwise it tries to guess it from
                the source or uses the default.
        """
        # parse() can not handle Path type in python 3.5
        if isinstance(source, Path):
            source = str(source)

        tree = parse(source, XMLParser(encoding=encoding))

        finsta03 = tree.find('FINSTA03')
        account_number = finsta03.findtext("S25_CISLO_UCTU", None)

        statement = BankStatement(account_number)

        finsta05 = finsta03.findall('FINSTA05')
        for item in finsta05:
            payment = Payment()

            payment.identifier = item.findtext('S28_POR_CISLO', None)
            payment.transaction_date = cls._parse_date(item)
            payment.counter_account = cls._parse_account(item)
            payment.name = item.findtext('PART_ACC_ID', None)
            payment.amount = cls._parse_amount(item)
            payment.description = item.findtext('PART_ID1_1', None)
            payment.constant_symbol = item.findtext('S86_KONSTSYM', None)
            payment.variable_symbol = item.findtext('S86_VARSYMOUR', None)
            payment.specific_symbol = item.findtext('S86_SPECSYMOUR', None)

            payment_code = cls._parse_code(item)
            if payment_code == 1:
                statement.add_payment(payment)

        return statement

    @staticmethod
    def _parse_code(item: Element) -> Optional[int]:
        code = item.findtext('S61_CD_INDIK')
        if code in ('D', 'C'):
            return 1
        elif code in ('DR', 'CR'):
            return 2
        else:
            raise ValueError('Unknown payment code.')

    @classmethod
    def _parse_date(cls, item: Element) -> Optional[date]:
        date_str = item.findtext('DPROCD', None)
        if date_str:
            return datetime.strptime(date_str, cls.date_format).date()
        else:
            return None

    @staticmethod
    def _parse_account(item: Element) -> Optional[str]:
        account_number = item.findtext('PART_ACCNO', None)
        bank_id = item.findtext('PART_BANK_ID', None)
        if account_number and bank_id:
            return '{}/{}'.format(account_number, bank_id)
        else:
            return None

    @staticmethod
    def _parse_amount(item: Element) -> Optional[Money]:
        amount = item.findtext('S61_CASTKA', None)
        if amount:
            return Money(amount.replace(',', '.'), 'CZK')
        else:
            return None
