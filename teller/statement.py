#
# Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
"""A module providing data classes to model bank statements."""
from datetime import date
from typing import Iterator, List, Optional

from moneyed import Money


class BankStatement:
    """Data class to model bank statement.

    Attributes:
        account_number: The number of the account for which was the statement produced.
        payments: The List of Payments in the given statement.
    """

    def __init__(self, account_number: Optional[str] = None):
        """Init BankStatement.

        Args:
            account_number: The number of the account for which was the statement produced.
        """
        self.account_number = account_number
        self.payments: List[Payment] = []

    def __repr__(self) -> str:
        return '<BankStatement: {}>'.format(self.account_number)

    def __iter__(self) -> Iterator['Payment']:
        return iter(self.payments)

    def add_payment(self, payment: 'Payment') -> None:
        """Add payment to the statement.

        Args:
            payment: The Payment to be added to the statement.
        """
        self.payments.append(payment)


class Payment:
    """Data class to model a transaction in bank statement.

    Attributes:
        identifier: Identifier of the payment.
        transaction_date: Transaction date.
        counter_account: Number of the counter account.
        name: Name associated with the counter account.
        amount: Amount transferred.
        description: Description of the payment provided by the payee.
        constant_symbol: Constant symbol identifying the payment.
        variable_symbol: Variable symbol identifying the payment.
        specific_symbol: Specific symbol identifying the payment.
    """

    def __init__(self, identifier: Optional[str] = None, transaction_date: Optional[date] = None,
                 counter_account: Optional[str] = None, name: Optional[str] = None, amount: Optional[Money] = None,
                 description: Optional[str] = None, constant_symbol: Optional[str] = None,
                 variable_symbol: Optional[str] = None, specific_symbol: Optional[str] = None):
        """Init Payment.

        Args:
            identifier: Identifier of the payment.
            transaction_date: Transaction date.
            counter_account: Number of the counter account.
            name: Name associated with the counter account.
            amount: Amount transferred.
            description: Description of the payment provided by the payee.
            constant_symbol: Constant symbol identifying the payment.
            variable_symbol: Variable symbol identifying the payment.
            specific_symbol: Specific symbol identifying the payment.
        """
        self.identifier = identifier
        self.transaction_date = transaction_date
        self.counter_account = counter_account
        self.name = name
        self.amount = amount
        self.description = description
        self.constant_symbol = constant_symbol
        self.variable_symbol = variable_symbol
        self.specific_symbol = specific_symbol

    def __repr__(self) -> str:
        return '<Payment: {}>'.format(self.identifier)

    def __eq__(self, other):
        return isinstance(other, Payment) \
               and self.identifier == other.identifier \
               and self.transaction_date == other.transaction_date \
               and self.counter_account == other.counter_account \
               and self.name == other.name \
               and self.amount == other.amount \
               and self.description == other.description \
               and self.constant_symbol == other.constant_symbol \
               and self.variable_symbol == other.variable_symbol \
               and self.specific_symbol == other.specific_symbol
