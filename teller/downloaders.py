#
# Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""A module providing downloaders of bank statements and possibly other resources."""

import codecs
import csv
import logging
from abc import ABC, abstractmethod
from datetime import datetime
from enum import Enum, unique
from io import BytesIO, StringIO, TextIOWrapper
from pathlib import Path
from time import sleep
from typing import Any, Iterable, Mapping, Optional, Sequence, Tuple, Union
from urllib.parse import urljoin
from warnings import warn

import requests
import zeep
from typing_extensions import Protocol
from zeep.proxy import ServiceProxy

LOGGER = logging.getLogger(__name__)


class TellerDownloadError(Exception):
    """Error raised by Teller when statement download fails."""


class RawStatement:
    """Data class to hold statement with its metadata.

    Attributes:
        content: The statement as downloaded from the bank.
        name: Name of the file where the statement was stored.
        encoding: Encoding of the statement.
    """

    def __init__(self, content: bytes, *, name: Optional[str] = None, encoding: Optional[str] = None):
        """Init RawStatement.

        Args:
            content: The statement as downloaded from the bank.
            name: Name of the file where the statement was stored.
            encoding: Encoding of the statement.
        """
        self.content = content
        self.name = name
        self.encoding = encoding

    def __eq__(self, other):
        return isinstance(other, RawStatement) \
            and self.content == other.content \
            and self.name == other.name \
            and self._compare_encoding(other)

    def _compare_encoding(self, other: 'RawStatement') -> bool:
        if isinstance(self.encoding, str) and isinstance(other.encoding, str):
            return self._normalize_encoding(self.encoding) == self._normalize_encoding(other.encoding)
        else:
            return self.encoding == other.encoding

    def __repr__(self):
        short = self.content if len(self.content) <= 15 else self.content[:12] + b'...'
        name = '' if self.name is None else ', name: {}'.format(self.name)
        encoding = '' if self.encoding is None else ', encoding: {}'.format(self.encoding)
        return '<RawStatement: {!r}{}{}>'.format(short, name, encoding)

    @classmethod
    def _normalize_encoding(cls, encoding: str) -> str:
        return codecs.lookup(encoding).name

    @property
    def buffer(self) -> BytesIO:
        """Return content as a byte stream.

        Returns:
            The content of the statement.
        """
        return BytesIO(self.content)


class BankStatementDownloader(ABC):
    """Abstract class for statement downloaders.

    Attributes:
        base_url: URL of the API.
        timeout: Timeout of the server requests.
    """

    def __init__(self, base_url: str, timeout: int = 3):
        """Init BankStatementDownloader.

        Args:
            base_url: URL of the API.
            timeout: Timeout of the server requests.
        """
        if not base_url.startswith('https://'):
            warn('Sensitive data is transferred over possibly insecure channel. Consider HTTPS.', UserWarning)
        if not base_url.endswith('/'):
            warn("base_url should end with '/' - otherwise it may be modified when the full URL is constructed.",
                 UserWarning)
        self.base_url = base_url
        self.timeout = timeout

    def get_statements(self, start_date: datetime, end_date: datetime) -> Sequence[RawStatement]:
        """Download the bank statements. DO NOT LOG EXCEPTIONS raised by this method. It may contain sensitive data.

        Args:
            start_date: Start of the interval from which should the statements be downloaded.
            end_date: End of the interval from which should the statements be downloaded.
        """
        self._check_arguments(start_date, end_date)
        return self._download_data(start_date, end_date)

    def _check_arguments(self, start_date: datetime, end_date: datetime) -> None:
        if not start_date <= end_date:
            raise ValueError('Start date is greater than end date.')

    @abstractmethod
    def _download_data(self, start_date: datetime, end_date: datetime) -> Sequence[RawStatement]:
        """Download content from a bank."""


class HttpStatementDownloader(BankStatementDownloader):
    """Download bank statements over http."""

    def _download_data(self, start_date: datetime, end_date: datetime) -> Sequence[RawStatement]:
        """Download the bank statements. DO NOT LOG EXCEPTIONS raised by this method. It may contain sensitive data."""
        try:
            url = self._compose_url(start_date, end_date)
            params = self._get_params(start_date, end_date)
            response = requests.get(url, params=params, timeout=self.timeout)
            response.raise_for_status()
        except Exception:
            raise TellerDownloadError('Statement could not be retrieved.')
        return (RawStatement(response.content, encoding=response.encoding),)

    def _compose_url(self, start_date: datetime, end_date: datetime) -> str:
        """Compose the bank specific URL from where the statement should be retrieved."""
        return self.base_url

    def _get_params(self, start_date: datetime, end_date: datetime) -> Optional[Mapping[str, Any]]:
        """Collect the bank specific request parametes."""
        return None


class FioStatementDownloader(HttpStatementDownloader):
    """Downloader for Fio bank statements."""

    date_format = '%Y-%m-%d'
    path_format = 'ib_api/rest/periods/{password}/{start}/{end}/transactions.xml'

    def __init__(self, base_url: str, password: str, **kwargs):
        super().__init__(base_url, **kwargs)
        self.password = password

    def _compose_url(self, start_date: datetime, end_date: datetime) -> str:
        start = start_date.strftime(self.date_format)
        end = end_date.strftime(self.date_format)
        path = self.path_format.format(password=self.password, start=start, end=end)
        return urljoin(self.base_url, path)


class RaiffeisenStatementDownloader(HttpStatementDownloader):
    """Downloader for Raiffeisen bank statements.

    Attributes:
        base_url: URL of the API.
        shopname: Shopname assigned to the customer by the bank to identify to the API.
        creditaccount: Creditaccount assigned to the customer by the bank to identify to the API.
        creditbank: Creditbank assigned to the customer by the bank to identify to the API.
        password: A password used to identify to the bank.
    """

    date_format = '%d.%m.%Y'
    path_format = 'ibs/eshop/payments-list'

    def __init__(self, base_url: str, shopname: str, creditaccount: int, creditbank: int, password: str,
                 **kwargs):
        """Init RaiffeisenStatementDownloader.

        Args:
            base_url: URL of the API.
            shopname: Shopname assigned to the customer by the bank to identify to the API.
            creditaccount: Creditaccount assigned to the customer by the bank to identify to the API.
            creditbank: Creditbank assigned to the customer by the bank to identify to the API.
            password: A password used to identify to the bank.
        """
        super().__init__(base_url, **kwargs)
        self.shopname = shopname
        self.creditaccount = creditaccount
        self.creditbank = creditbank
        self.password = password

    def _download_data(self, start_date: datetime, end_date: datetime) -> Tuple[RawStatement, ...]:
        statements = super()._download_data(start_date, end_date)
        return tuple(self._filter_account(self._strip_empty_text(statement)) for statement in statements)

    def _compose_url(self, start_date: datetime, end_date: datetime) -> str:
        return urljoin(self.base_url, self.path_format)

    def _get_params(self, start_date: datetime, end_date: datetime) -> Optional[Mapping[str, Any]]:
        params = {
            'shopname': self.shopname,
            'creditaccount': self.creditaccount,
            'creditbank': self.creditbank,
            'password': self.password,
            'paidfrom': start_date.strftime(self.date_format),
            'paidto': end_date.strftime(self.date_format),
            'listtype': 'PLAIN',
        }
        return params

    def _strip_empty_text(self, statement: RawStatement) -> RawStatement:
        """Turn string consisting of whitespace only to empty string.

        When no payment suits the filter criteria the bank returns a file consisting of one space. We prefer empty files
        in this case.
        """
        if statement.content.strip() == b'':
            return RawStatement(b'', name=statement.name, encoding=statement.encoding)
        else:
            return statement

    def _filter_account(self, statement: RawStatement) -> RawStatement:
        """Filter rows with payments to our target account.

        XXX: This whole function is really hacky, because we have to parse the
        raw statement before we can filter out rows. Unfortunatelly, we
        don't have anything better right now.
        """
        row_length = 16
        OWN_ACCOUNT_NO = 8
        OWN_ACCOUNT_BANK = 9

        reader = csv.reader(
            TextIOWrapper(BytesIO(statement.content), encoding=statement.encoding or 'utf-8'), delimiter=';',
        )
        output = StringIO()
        writer = csv.writer(output, delimiter=';', lineterminator='\n')
        for row in reader:
            # Raiffeisen bank includes other accounts than the requested in their statement.
            # Since Statement is designed to work with just one target account, we filter
            # those other results out.
            if len(row) == row_length:
                if (
                    row[OWN_ACCOUNT_NO].strip() != str(self.creditaccount)
                    or row[OWN_ACCOUNT_BANK].strip() != str(self.creditbank)
                ):
                    LOGGER.warning('Unexpected bank account {}/{} in statement for account {}/{}'.format(
                        row[OWN_ACCOUNT_NO], row[OWN_ACCOUNT_BANK], self.creditaccount, self.creditbank,
                    ))
                    continue

            writer.writerow(row)

        return RawStatement(
            output.getvalue().encode(statement.encoding or 'utf-8'),
            name=statement.name,
            encoding=statement.encoding,
        )


class CSOBStatementDownloader(BankStatementDownloader):
    """Downloader for CSOB bank statements.

    Attributes:
        base_url: URL of the API.
        wsdl_file: A path to the WSDL file containing the specification of the SOUP API.
        contract_number: Contract number assigned by the bank.
        cert: The certificate for the API.
        file_type: Type (format) of the statement which should be downloaded.
        statement_frequency: Specify whether to download daily or monthly statement.
        include_accounts: List of account numbers to be included. If set only statements for these accounts will be
            downloaded.
        exclude_accounts: List of account numbers to be excluded. Statements for the accounts listed in this parameter
            will not be downloaded.
        retries: If there are statements which should be downloaded but are not redy yet the downloader may wait for
            some time and retry. This parameter specifies the number of retries before the files are downloaded omitting
            the files which are not ready.
    """

    # Example of filenames on the server
    # '123456789_20210114_9_DCZB.xml'
    # '123456789_20210114_D_10_CZ.GPC'
    #
    #  Supposedly it stands for <account_no>_<date>_<frequency(D|M)>_<statement_no>_<currency>.gpc

    retry_interval = 3 * 60

    @unique
    class FileStatus(str, Enum):
        NOT_READY = 'R'
        READY = 'D'
        ERROR = 'F'

    @unique
    class FileType(str, Enum):
        XML = 'XML'
        GPC = 'BBGPC'

    @unique
    class StatementFrequency(str, Enum):
        DAILY = '_D_'
        MONTHLY = '_M_'

    class FileDetailProtocol(Protocol):
        # py35 can not handle "var: str" syntax and "var #type: str" is invalid  without assignment
        Filename = ''  # type: str
        Status = ''  # type: str
        Url = ''  # type: str

    def __init__(self, base_url: str, wsdl_file: Union[str, Path], contract_number: Optional[str] = None,
                 cert_path: Optional[Union[str, Path]] = None, key_path: Optional[Union[str, Path]] = None,
                 file_type: FileType = FileType.XML, statement_frequency: Optional[StatementFrequency] = None,
                 include_accounts: Optional[Iterable[str]] = None, exclude_accounts: Optional[Iterable[str]] = None,
                 retries: int = 0, **kwargs):
        """Init CSOBStatementDownloader.

        Args:
            base_url: URL of the API.
            wsdl_file: A path to the WSDL file containing the specification of the SOUP API.
            contract_number: Contract number assigned by the bank.
            cert_path: Path to the certificate file.
            key_path: Path to the key file.
            file_type: Type (format) of the statement which should be downloaded.
            statement_frequency: Specify whether to download daily or monthly statement.
            include_accounts: List of account numbers to be included. If set only statements for these accounts will be
                downloaded.
            exclude_accounts: List of account numbers to be excluded. Statements for the accounts listed in this
                parameter will not be downloaded.
            retries: If there are statements which should be downloaded but are not redy yet the downloader may wait for
                some time and retry. This parameter specifies the number of retries before the files are downloaded
                omitting the files which are not ready.
        """
        super().__init__(base_url, **kwargs)
        self.wsdl_file = str(wsdl_file)
        self.contract_number = contract_number

        # Passing a single str as ex/include_accounts results in adding many one char account names, not the one passed
        if isinstance(exclude_accounts, str):
            raise ValueError('Parameter exclude_accounts should be a list of strings not a single string.')
        if isinstance(include_accounts, str):
            raise ValueError('Parameter include_accounts should be a list of strings not a single string.')
        self.exclude_accounts = set(exclude_accounts or [])
        self.include_accounts = set(include_accounts) if include_accounts is not None else None

        if cert_path is not None and key_path is not None:
            self.cert = (str(cert_path), str(key_path))  # type: Union[Tuple[str, str], None]
        else:
            self.cert = None

        self.file_type = file_type

        if statement_frequency is not None and file_type != self.FileType.GPC:
            raise ValueError('Parameter "statement_frequency" only alowed for GPC statements.')
        else:
            self.statement_frequency = statement_frequency

        self.retries = retries

    def _download_data(self, start_date: datetime, end_date: datetime) -> Tuple[RawStatement, ...]:
        result = []

        statement_details = list(self._get_statement_details(start_date, end_date))
        retries = 0
        while not all(ready for _, _, ready in statement_details) and (retries < self.retries):
            LOGGER.info('Not all statements ready. Retry downloading statement details.')
            sleep(self.retry_interval)
            statement_details = list(self._get_statement_details(start_date, end_date))
            retries += 1

        for url, filename, ready in statement_details:
            if ready:
                try:
                    response = requests.get(url, cert=self.cert, timeout=self.timeout)
                    response.raise_for_status()
                    result.append(RawStatement(response.content, name=filename, encoding=response.encoding))
                except Exception:
                    raise TellerDownloadError('Statement at {} could not be obtained.'.format(url))
            else:
                LOGGER.warning('Could not get file {} - file not ready.'.format(filename))

        return tuple(result)

    def _get_statement_details(self, start_date: datetime, end_date: datetime) -> Iterable[Tuple[str, str, bool]]:
        service = self._create_service()

        filter_params = {'FileTypes': ['VYPIS'],
                         'FileFormats': [self.file_type.value],
                         'CreatedAfter': start_date,
                         'CreatedBefore': end_date}
        try:
            response = service.GetDownloadFileList_v3(self.contract_number, Filter=filter_params)
        except Exception:
            raise TellerDownloadError('Could not obtain file list.')

        if response.FileList:
            files_details = response.FileList.FileDetail  # type: Iterable[CSOBStatementDownloader.FileDetailProtocol]
        else:
            files_details = []

        for detail in files_details:
            if self._check_statement_frequency(detail) and self._is_included(detail):
                yield detail.Url, detail.Filename, self._is_ready(detail)

    def _check_statement_frequency(self, file_detail: FileDetailProtocol) -> bool:
        if self.statement_frequency is None:
            return True
        else:
            # The type of the statement (daily/monthly) is indicated by presence of "_D_" or "_M_" substring
            # in the filename.
            return self.statement_frequency.value in file_detail.Filename

    def _is_ready(self, file_detail: FileDetailProtocol) -> bool:
        if file_detail.Status == self.FileStatus.READY:
            return True
        elif file_detail.Status == self.FileStatus.NOT_READY:
            return False
        elif file_detail.Status == self.FileStatus.ERROR:
            raise TellerDownloadError('Could not get file {} - error on the bank side.'.format(file_detail.Filename))
        else:
            raise TellerDownloadError('Unknown status of file {}.'.format(file_detail.Filename))

    def _is_included(self, file_detail: FileDetailProtocol) -> bool:
        account = file_detail.Filename.split('_', 1)[0]
        if account in self.exclude_accounts:
            return False
        return (self.include_accounts is None) or (account in self.include_accounts)

    def _create_service(self) -> ServiceProxy:
        session = requests.Session()
        session.cert = self.cert
        transport = zeep.transports.Transport(session=session)
        client = zeep.Client(self.wsdl_file, transport=transport)
        service = client.create_service('{http://ceb-bc.csob.cz/CEBBCWS}CEBBCWSBinding', self.base_url)
        return service
