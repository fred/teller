===========
Fred-teller
===========

Python library for downloading and conversion of bank statements.

Usual workflow is like this::

    downloader = BankStatementDownloader(**params)
    raw_statements = downloader.get_statements(start_date, end_date)
    statement = BankStatementParser.parse_file(raw_statement.buffer)

where ``params`` are downloader specific parameters (see bellow) and ``start_date`` and ``end_date`` specify
the interval for which the statements should be downloaded. ``BankStatementDownloader`` and ``BankStatementParser``
should be replaced by concrete implementations for the concrete bank.

Downloaders
===========
Module containing classes used to download data from banks.

``RawStatement``
----------------
An auxiliary class holding the raw data downloaded from the bank along with some metadata such as encoding of
the statement.

``FioStatementDownloader``
--------------------------
Downloader used to download statements from Fio bank.

Parameters

:base_url: URL of the API. Usually ``https://www.fio.cz/``.
:password: A password used to identify to the bank.

``RaiffeisenStatementDownloader``
---------------------------------
Downloader used to download statements from Raiffeisen bank.

Parameters

:base_url: URL of the API. Usually ``https://online.rb.cz/``.
:shopname: A parameter assigned to the customer by the bank to identify to the API.
:creditaccount: A parameter assigned to the customer by the bank to identify to the API.
:creditbank: A parameter assigned to the customer by the bank to identify to the API.
:password: A password used to identify to the bank.

``CSOBStatementDownloader``
---------------------------
Downloader used to download statements from ČSOB bank.

Parameters

:base_url: URL of the API. Usually ``https://ceb-bc.csob.cz/cebbc/api/``. **TODO: Is it the whole URL including
           the path?**
:wsdl_file: A path to the ``WSDL`` file containing the specification of the SOUP API
:contract_number: Contract number assigned by the bank.
:cert_path: Path to the certificate file.
:key_path: Path to the key file.
:file_type: Type (format) of the statement which should be downloaded specified as a member of
            ``CSOBStatementDownloader.FileType`` enum.
:statement_frequency: Specify whether to download daily or monthly statement. Member of
                      ``CSOBStatementDownloader.StatementFrequency`` enum.
:include_accounts: List of account numbers to be included. If set only statements for these accounts will be downloaded.
:exclude_accounts: List of account numbers to be excluded. Statements for the accounts listed in this parameter will
                   not be downloaded.
:retries: If there are statements which should be downloaded but are not ready yet the downloader may wait for some time
          and retry. This parameter specifies the number of retries before the files are downloaded omitting the files
          which are not ready.

Parsers
=======
Module containing classes used to parse ``RawStatement`` containing the raw data downloaded from the banks.
Available parsers are:

:FioParser: for Fio bank
:RaiffeisenParser: for Raiffeisen bank
:CSOBParser: for ČSOB

Parsers are parameterless. The method ``parse_file`` optionally accepts encoding of the data contained in
the ``RawStatement``.

Statement
=========
Module containing class ``BankStatement`` which holds data about a bank statement and ``Payment`` which holds data about
an individual payment.
